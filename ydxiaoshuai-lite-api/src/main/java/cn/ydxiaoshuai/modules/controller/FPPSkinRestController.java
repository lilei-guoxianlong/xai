package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 小帅丶
 * @className FPPSkinRestController
 * @Description Face++
 * @Date 2020/9/24-13:43
 **/
@Slf4j
@Controller
@RequestMapping(value = "/rest/fpp")
@Scope("prototype")
@Api(tags = "Face++识别API")
public class FPPSkinRestController extends ApiRestController {

}
