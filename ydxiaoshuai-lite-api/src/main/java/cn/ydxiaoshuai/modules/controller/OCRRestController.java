package cn.ydxiaoshuai.modules.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 小帅丶
 * @className OCRRestController
 * @Description 文字识别
 * @Date 2020/9/24-16:00
 **/
@Slf4j
@Controller
@RequestMapping(value = "/rest/icr")
@Scope("prototype")
@Api(tags = "文字识别API")
public class OCRRestController {
}
