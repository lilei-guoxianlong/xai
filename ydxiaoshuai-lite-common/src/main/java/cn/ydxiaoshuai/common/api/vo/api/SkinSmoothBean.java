package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className SkinSmoothBean
 * @Description 皮肤光滑度接口返回页面的对象
 * @Date 2020/4/10-15:11
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SkinSmoothBean extends BaseBean{
    //具体返回的内容
    private Data data;
    @lombok.Data
    public static class Data{
        private String image_base64;
        private String smooth;
        private String color;
        private Integer wrinkle_num;
        private PalmPollingXYBean.DataBean.LinesBean.LLBean LL;
        private PalmPollingXYBean.DataBean.LinesBean.WLBean WL;
        private PalmPollingXYBean.DataBean.LinesBean.ALBean AL;


        private WSPalmPollingXYBean.LinesBean.LLBean LLWS;
        private WSPalmPollingXYBean.LinesBean.WLBean WLWS;
        private WSPalmPollingXYBean.LinesBean.ALBean ALWS;
        private WSPalmPollingXYBean.LinesBean.CLBean CLWS;

        private String prefix;

    }
    public SkinSmoothBean success(String msg, String msg_zh, Data data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public SkinSmoothBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public SkinSmoothBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
