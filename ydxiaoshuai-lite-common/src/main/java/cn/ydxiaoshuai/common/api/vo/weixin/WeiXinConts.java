package cn.ydxiaoshuai.common.api.vo.weixin;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 小帅丶
 * @className WeiXinConts
 * @Description 微信appid 微信appsecret 微信access_token
 * @Date 2020/9/10-16:50
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeiXinConts {
    private String appid;
    private String appsecret;
    private String access_token;
    private String update_date = DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN);

    public WeiXinConts(String appid, String appsecret) {
        this.appid = appid;
        this.appsecret = appsecret;
    }

    public WeiXinConts(String appid, String appsecret, String access_token) {
        this.appid = appid;
        this.appsecret = appsecret;
        this.access_token = access_token;
    }
}
