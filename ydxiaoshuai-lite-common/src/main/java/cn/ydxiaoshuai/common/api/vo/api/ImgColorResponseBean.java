package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className ImgColorResponseBean
 * @Description 颜色识别接口返回对象
 * @Date 2020/11/2-14:04
 **/
@NoArgsConstructor
@Data
public class ImgColorResponseBean {


    private String code;
    private boolean charge;
    private int remain;
    private int remainTimes;
    private int remainSeconds;
    private String msg;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private String status_code;
        private String status_message;
        private List<ColorListBean> color_list;

        @NoArgsConstructor
        @Data
        public static class ColorListBean {

            private String hex;
            private double percentage;
            private String rgb;
        }
    }
}
