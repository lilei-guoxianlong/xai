package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className ImageClassifyResponseBean
 * @Description 图像识别接口返回
 * @Date 2020/9/24-14:20
 **/
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageClassifyGeneralResponse extends BaiDuBase{

    private int result_num=0;
    private List<Result> result;
    /**车型识别会有此字段*/
    private String color_result="";
    /**车型识别会有此字段*/
    private CarLocation location_result;


    @NoArgsConstructor
    @Data
    public static class CarLocation {

        private double top;
        private double left;
        private double width;
        private double height;
    }

    @NoArgsConstructor
    @Data
    public static class Result {

        private String probability;
        private boolean has_calorie;
        private String calorie;
        private String name;
        private BaikeInfo baike_info;
        /**LOGO会有此字段*/
        private Integer type;
        /**LOGO会有此字段*/
        private Location location;
        /**PLANT会有此字段*/
        private double score;
        /**车型识别会有此字段*/
        private String year;

        @NoArgsConstructor
        @Data
        public static class BaikeInfo {

            private String baike_url;
            private String image_url;
            private String description;
        }

        @NoArgsConstructor
        @Data
        public static class Location {

            private Integer top;
            private Integer left;
            private Integer width;
            private Integer height;
        }
    }
}
