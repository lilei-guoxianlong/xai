package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

import java.util.List;

/**
 * @Description 获取ORDER返回的bean
 * @author 小帅丶
 * @className PalmOrdersBean
 * @Date 2020/1/3-10:48
 **/
@Data
public class PalmOrdersBean {

    /**
     * order_id : SXYJ157801822200800040
     * channel : swdywlcm000
     * schannel :
     * amount : 9.99
     * origin_amount : 59.99
     * status : created
     * phone :
     * is_send_phone : null
     * email :
     * is_send_email : null
     * created_at : 2020-01-03 10:23:42
     * is_free : no
     * attributes : [{"palm_id":"7d3c4b748cbf42a3b4a5bf0ecd941565202001","name":"","is_share":"0","index_version":"1"}]
     * next : /pay/shouxiangyanjiuyuan?order_id=SXYJ157801822200800040
     */

    private String order_id;
    private String channel;
    private String schannel;
    private double amount;
    private double origin_amount;
    private String status;
    private String phone;
    private Object is_send_phone;
    private String email;
    private Object is_send_email;
    private String created_at;
    private String is_free;
    private String next;
    private List<AttributesBean> attributes;

    @Data
    public static class AttributesBean {
        /**
         * palm_id : 7d3c4b748cbf42a3b4a5bf0ecd941565202001
         * name :
         * is_share : 0
         * index_version : 1
         */
        private String palm_id;
        private String name;
        private String is_share;
        private String index_version;

    }
}
