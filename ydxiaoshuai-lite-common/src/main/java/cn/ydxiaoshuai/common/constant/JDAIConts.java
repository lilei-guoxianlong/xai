package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className GarbageConts
 * @Description 垃圾分类查询常量
 * @Date 2020/4/26-16:40
 **/
public class JDAIConts {

    public static enum IMAGE_TYPE{
        BASE64,URL,FACE_TOKEN
    };
    //京东AI接口地址
    public static String GARBAGEIMAGESEARCH_URL = "https://aiapi.jd.com/jdai/garbageImageSearch";
    /** 接口地址-颜色识别 */
    public static String EXTRACT_IMG_COLORS_URL = "https://aiapi.jd.com/jdai/extract_img_colors";
    /** 接口地址-搭配生成 */
    public static String RECOMMEND_OUTFITS_URL = "https://aiapi.jd.com/jdai/recommend_outfits";

    //京东AI应用-APPKEY
    public static String APPKEY = "";
    //京东AI应用-SECRETKEY
    public static String SECRETKEY = "";

    /** 查询成功 */
    public static String STATUS_CODE_0 = "0";
    //查询成功
    public static String CODE_10000 = "10000";
    //错误的请求appkey
    public static String CODE_10001 = "10001";
    //不存在相应的数据信息
    public static String CODE_10003 = "10003";
    //URL上appkey参数不能为空
    public static String CODE_10004 = "10004";
    //接口需要付费，请充值
    public static String CODE_10010 = "10010";
    //系统繁忙，请稍后再试
    public static String CODE_10020 = "10020";
    //调用网关失败，请与NeuHub联系
    public static String CODE_10030 = "10030";
    //超过每天限量，请明天继续
    public static String CODE_10040 = "10040";
    //URL上timestamp参数不能为空
    public static String CODE_10041 = "10041";
    //URL上sign参数不能为空
    public static String CODE_10042 = "10042";
    //超过QPS限额，请降低调用频率或与NeuHub联系
    public static String CODE_10043 = "10043";
    //集群QPS超限额，请与NeuHub联系
    public static String CODE_10044 = "10044";
    //timestamp参数无效，请检查timestamp距离当前时间是否超过5分钟
    public static String CODE_10045 = "10045";
    //timestamp参数格式错误
    public static String CODE_10046 = "10046";
    //请求签名sign无效，请检查签名信息
    public static String CODE_10047 = "10047";
    //无接口权限，请下单购买
    public static String CODE_10048 = "10048";
    //超过每天最大调用量
    public static String CODE_10049 = "10049";
    //用户已被禁用
    public static String CODE_10050 = "10050";
    //发布方设置调用权限，请联系发布方
    public static String CODE_10060 = "10060";
    //文件大小超限，请上传小于5M的文件
    public static String CODE_10090 = "10090";
    //发布方接口调用异常，请稍后再试
    public static String CODE_11010 = "11010";
    //发布方接口返回格式有误
    public static String CODE_11030 = "11030";
}
