package cn.ydxiaoshuai.modules.weixin.service.impl;

import cn.ydxiaoshuai.common.api.vo.weixin.WeiXinConts;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.weixin.entity.WeixinAccount;
import cn.ydxiaoshuai.modules.weixin.mapper.WeixinAccountMapper;
import cn.ydxiaoshuai.modules.weixin.service.IWeixinAccountService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信公众账号表
 * @Author: 小帅丶
 * @Date:   2020-09-10
 * @Version: V1.0
 */
@Service
public class WeixinAccountServiceImpl extends ServiceImpl<WeixinAccountMapper, WeixinAccount> implements IWeixinAccountService {
    @Autowired
    private WeixinAccountMapper weixinAccountMapper;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * @Author 小帅丶
     * @Description 根据内部编码查询信息并放在Redis
     * @Date  2020/9/10 17:09
     * @param account_code
     * @return void
     **/
    @Override
    public void refreshOneAppid(String account_code) {
        LambdaQueryWrapper<WeixinAccount> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WeixinAccount::getAccountCode, account_code);
        WeixinAccount weixinAccountDB = weixinAccountMapper.selectOne(queryWrapper);
        if(null!=weixinAccountDB){
            redisUtil.set(account_code, new WeiXinConts(weixinAccountDB.getAccountAppid(),weixinAccountDB.getAccountAppsecret()));
        }
    }
}
