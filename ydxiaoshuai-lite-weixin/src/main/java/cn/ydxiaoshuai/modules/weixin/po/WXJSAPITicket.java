package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description jsapi_ticket
 * @author 小帅丶
 * @className WXJSAPITicket
 * @Date 2019/11/28-11:06
 **/
@Data
public class WXJSAPITicket extends WXErrorGlobal{
    /** 凭证有效时间，单位：秒  默认2小时 7200s*/
    private String expires_in;
    /** 获取到JSAPI的凭证 */
    private String ticket;
}
