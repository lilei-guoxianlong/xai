package cn.ydxiaoshuai.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.system.entity.SysUserAgent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 用户代理人设置
 * @Author: 小帅丶
 * @Date:  2019-04-17
 * @Version: V1.0
 */
public interface SysUserAgentMapper extends BaseMapper<SysUserAgent> {

}
